
import '../styles/App.css';

import { createBrowserRouter, RouterProvider } from 'react-router-dom'

/** Import Components */
import Main from './Main';
import GameSelection from './GameSelection';
import StoryLineChooseCharacter from './StoryLineChooseCharacter';
import StoryLineGame from './StoryLineGame';
import Quiz from './Quiz';
import Result from './Result';

/** React Routers */
const router = createBrowserRouter([
  {
    path : '/',
    element : <Main></Main>
  },
  {
    path : '/gameselection',
    element : <GameSelection></GameSelection>
  },
  {
    path : '/storylinechoosecharacter',
    element : <StoryLineChooseCharacter></StoryLineChooseCharacter>
  },
  {
    path : '/storylinegame',
    element : <StoryLineGame></StoryLineGame>
  },
  {
    path : '/quiz',
    element : <Quiz></Quiz>
  },
  {
    path : '/result',
    element : <Result></Result>
  }
])

function App() {
  return (
    <>
     <RouterProvider router={router}/>
    </>
  );
}

export default App;
