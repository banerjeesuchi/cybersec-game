import React, { useRef } from "react";

import '../styles/Main.css' 

import { Link } from "react-router-dom";

export default function Main() {

    const inputRef = useRef(null)


    return(
        <div className="container">
            <h1 className="text-light">Learn Something New About Cyber-Security by Playing</h1>
            <h3 className="text-light">Word of Caution</h3>
            <p className="text-light">
            Characters here are imaginary and any website name, gateway names here are just to indicate the example of how fraudsters can fake with and to bring the awareness on if you notice the misspelled word or words that’s are not real or color that is different than the actual logo etc., these can indicate they are not safe. Intension of this game is purely an attempt to create awareness. Suggestions or situations mentioned here are not a permanent solution and we hope that these tough times or fraudulent acts do not happen with you. Please be careful and spread awareness amongst your family and friends.
            </p>
            {/* <ol>
                <li>You will be shown 10 messgaes one after another</li>
                <li>20 pts will be awarded and 20% Health will be reduced for attacker for correctly identifying Fake vs Not Fake messages</li>
                <li>20% Health will be reduced if you incorrectly indentifed the message</li>
                <li>Best of Luck!!!</li>

            </ol> */}

            {/* <form id ="form">
                <input ref={inputRef} className="userid" type="text" placeholder="Username*" />
            </form> */}

            <div className="start">
                <Link className="btn" to={'gameselection'}>Start Game</Link>
            </div>

        </div>
    )
}   