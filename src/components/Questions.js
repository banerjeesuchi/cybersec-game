import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";

/**Custom Hook  */
import { useFetchQuestion } from "../hooks/FetchQuestions";
import { updateResult } from "../hooks/setResult";

export default function Questions() {

    

    const [{isLoading,apiData,serverError},] = useFetchQuestion()


    const questions = useSelector(state => state.questions.queue[state.questions.trace]);
    const { queue, trace, score, health } = useSelector(state => state.questions);



    // useEffect(() => {
        // dispatch(updateResult({ trace, checked }))
        // console.log(queue[trace])
    // })

    let StoryRen
    if(queue[trace]?.image !== ''){
        // console.log(queue[trace]?.image)
        StoryRen = <div className="grid">
                        {/* <div className="storyquestion">
                            <p className="text-light"> {questions?.story}</p>
                            <p className="text-light"> {questions?.message}</p>
                        </div>
                        <img src={ queue[trace]?.image } className="questionimages"/> */}
                        <div className="question-div">
                            <p className="text-light">{questions?.story}</p>
                            <br/><br/>
                            <p className="text-light question-text">{questions?.message}</p>
                        </div>
                        <div>
                            <img src={ queue[trace]?.image } className="ques-images"/>
                        </div>
                    </div>
    }
    else{
        StoryRen = <p className="text-light"> {questions?.message}</p>
    }

    if(isLoading) return <h3 className="text-light">Loading</h3>
    if(serverError) return <h3 className="text-light">{serverError || "Unknown Error"}</h3>

    return(
        <div className="questions">
            {StoryRen}
        </div>
       
        // <div className="questions grid">
        //     <p className="text-light">{questions?.message}</p>
        // </div>
    )
}   