import React, { useEffect,useState } from "react";
import { useSelector,useDispatch } from 'react-redux'

const ProgressBar = () => {
    const bgcolor  = "#d43141";

    const game = useSelector(state => state.game.game)
    const g1_score = useSelector(state => state.questions.score);
    const g2_score = useSelector(state => state.story_line_questions.score);

    let score
    if(game === 'FakeBook')
    {
      score = g1_score
    }
    else if(game === 'StoryLine')
    {
      score = g2_score
    }
    

    const containerStyles = {
      height: 40,
      width: '30%',
      backgroundColor: "#e0e0de",
      borderRadius: 10,
      margin: 50,
      justifySelf:"center",
    }
  
    const fillerStyles = {
      height: '100%',
      width: `${100-score}%`,
      backgroundColor: bgcolor,
      borderRadius: 'inherit',
      textAlign: 'right',
      transition: 'width 1s ease-in-out',
    }
  
    const labelStyles = {
      padding: 20,
      color: 'black',
      fontWeight: 'bold',

    }
  
    return (
      <div style={containerStyles}>
        <div style={fillerStyles}>
          <span style={labelStyles}>{`${100-score}%`}</span>
        </div>
      </div>
    );
  };
  
  export default ProgressBar;