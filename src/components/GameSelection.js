import React from 'react';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux'
import { useNavigate } from "react-router-dom";
import { resetAllAction } from '../redux/question_reducer';
import { resetResultAction } from '../redux/result_reducer';
import { SetGame } from '../hooks/SetGame';
import '../styles/GameSelection.css';
import { resetStoryLineAllAction } from '../redux/story_line_question_reducer';

export default function GameSelection() {

  const dispatch = useDispatch();

  
  let navigate1 = useNavigate(); 
  const routeChangeSS = () =>{ 
    console.log("am I here too")
    dispatch(SetGame("FakeBook"));
    let path = '/quiz'; 
    navigate1(path);
  }

  let navigate2 = useNavigate(); 
  const routeChangeST = () =>{ 
    dispatch(SetGame("StoryLine"))
    let path = '/storylinechoosecharacter'; 
    navigate2(path);
  }

  useEffect(() => {
      dispatch(resetAllAction())
      dispatch(resetStoryLineAllAction())
      dispatch(resetResultAction())
  })

  return (
    <div className='container'>
        <h1 className='text-light' style={ {textAlign:'center'} }>Choose a Game to play!</h1>
        <div className='grid'>
          <div className='card card-1' onClick={routeChangeSS}>
            <img src={require(`../Images/fakebook.png`)} alt='fakebook-game' className='card-img'/>
            <h3 className='card-text'>SMVISHING</h3>
          </div>

          <div className='card card-2' onClick={routeChangeST}>
            <img src={require(`../Images/storyline.png`)} alt='storyline-game' className='card-img'/>
            <h3 className='card-text'>KHELTE</h3>
          </div>
        </div>
    </div>  
  )
}
