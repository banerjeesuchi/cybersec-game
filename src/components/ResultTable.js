import React from 'react'
import { useSelector } from 'react-redux';
import { useEffect } from 'react';

export default function ResultTable() {

    const game = useSelector(state => state.game.game)
    const g1_info = useSelector(state => state.questions.queue);
    const g2_info = useSelector(state => state.story_line_questions.queue);
    const user_answer = useSelector(state => state.result.result)


    useEffect(()=>{
        console.log(g2_info)
    })

    let g_info
    if(game === 'FakeBook'){
        g_info = g1_info
    }else if(game === 'StoryLine'){
        g_info = g2_info
    }

    const DisplayData=user_answer.map(
        (info,i)=>{
            let temp_remark,temp_qn
            if(game === 'FakeBook'){
                temp_qn = g_info[i].message
            }else if(game === 'StoryLine'){
                temp_qn = g_info[i].question
            }

            if(user_answer[i] === g_info[i].answer){
                temp_remark = g_info[i].yes_text
            }else{
                temp_remark = g_info[i].no_text
            }

            return(
                <tr>
                    <td className=''>{temp_qn}</td>
                    <td className=''>{user_answer[i]}</td>
                    <td className=''>{temp_remark}</td>
                </tr>
            )
        }
    )
    

    return(
        <div>
            <table>
                <thead className='table-header'>
                    <tr className='table-row'>
                        <td>Question</td>
                        <td>Your Answer</td>
                        <td>Remark</td>
                    </tr>
                </thead>
                <tbody className='table-body'>
                    {DisplayData}
                    
                </tbody>
            </table>
        </div>


    )
}