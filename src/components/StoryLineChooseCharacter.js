import React from 'react';
import { useEffect } from 'react';
import { useSelector,useDispatch } from 'react-redux'
import { useNavigate } from "react-router-dom";

import * as Action from '../redux/story_line_character_reducer'

import { SetStoryLineCharacter } from '../hooks/SetStoryLineCharacter';

export default function StoryLineChooseCharacter() {

  const dispatch = useDispatch();
  
  let navigate1 = useNavigate(); 
  const ChG = () =>{ 
    dispatch(SetStoryLineCharacter("G"))
    let path = '/storylinegame'; 
    navigate1(path);
  }

  let navigate2 = useNavigate(); 
  const ChB = () =>{ 
    dispatch(SetStoryLineCharacter("B"))
    let path = '/storylinegame'; 
    navigate2(path);
  }

//   useEffect(() => {
//   })



  return (
    <div className='container'>
      <h1 className='text-light' style={ {textAlign:'center'} }>Choose your Player</h1>
        <div className='grid'>
          <div className='card card-1' onClick={ChG}>
            <img src={require(`../Images/girl_new.png`)} alt='girl' className='card-img'/>
            <h3 className='card-text'>SHRIYA</h3>
          </div>

          <div className='card card-2' onClick={ChB}>
            <img src={require(`../Images/boy_new.png`)} alt='boy' className='card-img'/>
            <h3 className='card-text'>SHREYANSH</h3>
          </div>
          {/* <h3 className="text-easy characterg" onClick={ChG}>Shriya</h3>
          <h3 className="text-hard characterb" onClick={ChB}>Shreyansh</h3> */}
        </div>
    </div>  
  )
}
