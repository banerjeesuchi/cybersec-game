import React, { useEffect, useState } from "react";
import StoryLineQuestions from "./StoryLineQuestions";

import { useDispatch, useSelector } from "react-redux";

import { Navigate } from 'react-router-dom'
import ProgressBar from "./ProgressBar";
import E_ProgressBar from "./E_ProgressBar";

import hacker from '../Images/hacker.jfif'
import g_i from '../Images/girl_new.png'
import b_i from '../Images/boy_new.png'


import { IncrementScore, MoveNextQuestion, DecreaseHealth } from "../hooks/FetchStoryLineQuestions";
import { PushAnswer } from "../hooks/setResult";


export default function StoryLineGame() {

    const [fire, setFire] = React.useState(0)

    const [fire_e, setFire_e] = React.useState(0)


    const state = useSelector(state=>state);
    const { queue, trace, score, health } = useSelector(state => state.story_line_questions);
    const { character } = useSelector(state=>state.story_line_character)
    const result   =  useSelector(state=>state.result.result)

    const dispatch = useDispatch();


    useEffect(() => {
        // console.log(result)
    })


    function onSafe(){
        console.log('On Safe Click')
        /**increase trace value by 1 using move next action*/
        if(trace<queue.length){
            if(queue[trace].answer == "Safe"){
                setFire(1)
                dispatch(IncrementScore());
            }else{
                setFire_e(1)
                dispatch(DecreaseHealth());
            }

            dispatch(MoveNextQuestion());
            
            if(result.length <= trace){
                dispatch(PushAnswer("Safe"))
            }
            
        }
    }
    
    

    function onNotSafe(){
        console.log('On Not Safe Click')
        /**increase trace value by 1 using move next action*/
        if(trace<queue.length){
            if(queue[trace].answer == "Not Safe"){
                setFire(1)
                dispatch(IncrementScore());
            }else{
                setFire_e(1)
                dispatch(DecreaseHealth());
            }

            dispatch(MoveNextQuestion());

            if(result.length <= trace){
                dispatch(PushAnswer("Not Safe"))
            }
        
        }
    }

    if((result.length && result.length >= queue.length)|| health === 0 || score === 100){
        return <Navigate to={'/result'} replace={true}></Navigate>
    }


    let img_ch
    if(character==="G"){
        img_ch=g_i
    }
    else{
        img_ch=b_i
    }
    return(
        <div className="container">
            <div className="grid3">
                <h5 className="text-light">Identify Fake or Not</h5>
                
                <h5 className="text-light" style={{justifySelf:"center"}}>Your Score : {score}</h5>
            
            
                {/* <Countdown
                        
                        key={k}
                        date={Date.now() + 20000}
                        intervalDelay={0}
                        precision={3}
                        renderer={timerRenderer}
                        onComplete={onCompleteTimeFun}
                    /> */}
            </div>
            <div className="grid">
                <ProgressBar></ProgressBar>
                <E_ProgressBar></E_ProgressBar>
            </div>

            <div  className="grid3">
                <img src={img_ch} alt="Character" className="images"/>
                <div className="fire_grid">
                    <div className="Fire" onAnimationEnd={() => setFire(0)} fire={fire}></div>
                    <div className="Fire_E" onAnimationEnd={() => setFire_e(0)} fire_e={fire_e}></div>
                </div>
                <img src={hacker} alt="Attacker" className="images" />
            </div>
            
            <StoryLineQuestions/>
            
            

            <div className="grid">
                <button className="btn next" onClick={onNotSafe} >{queue[trace]?.options[0]}</button>
                <button className="btn prev" onClick={onSafe} >{queue[trace]?.options[1]}</button>
            </div>
    

        </div>
    )

    /**finished after last question or health */
}   