import React, { useEffect,useState } from "react";
import Countdown from 'react-countdown';
import Questions from "./Questions";

import { IncrementScore, MoveNextQuestion, DecreaseHealth } from "../hooks/FetchQuestions";
import { PushAnswer,updateResult } from "../hooks/setResult";

/**redux store import */
import { useSelector,useDispatch } from 'react-redux'
import { Navigate } from 'react-router-dom'
import ProgressBar from "./ProgressBar";
import E_ProgressBar from "./E_ProgressBar";

import SS1 from '../Images/SS1.png'
import SS2 from '../Images/SS2.png'
import { resetResultAction } from "../redux/result_reducer";
import { resetAllAction } from "../redux/question_reducer";


export default function Quiz() {

    const { queue, trace, score, health } = useSelector(state => state.questions);

    const [fire, setFire] = React.useState(0)

    const [fire_e, setFire_e] = React.useState(0)

    // Timer vars:


    
    const state = useSelector(state=>state);

    const result   =  useSelector(state=>state.result.result)
    const dispatch = useDispatch();

    const [k, setK] = useState(false);

    
    
    let question

    useEffect(() => {
        console.log(queue)
        // dispatch(resetAllAction())
        // dispatch(resetResultAction())
    })



    /**finished after last question or health */

    if((result.length && result.length >= queue.length)|| health === 0 || score === 100){
        return <Navigate to={'/result'} replace={true}></Navigate>
    }



    /** Next Button Event handler */
    function onFake(){
        console.log('On Fake Click')
        /**increase trace value by 1 using move next action*/
        if(trace<queue.length){
            if(queue[trace].answer == "Fake"){
                setFire(1)
                dispatch(IncrementScore());
            }else{
                setFire_e(1)
                dispatch(DecreaseHealth());
            }

            dispatch(MoveNextQuestion());
            
            if(result.length <= trace){
                dispatch(PushAnswer("Fake"))
            }
        }
    }
    

    function onNotFake(){
        console.log('On Not Fake Click')
        /**increase trace value by 1 using move next action*/
        if(trace<queue.length){
            if(queue[trace].answer == "Real"){
                setFire(1)
                dispatch(IncrementScore());
            }else{
                setFire_e(1)
                dispatch(DecreaseHealth());
            }

            dispatch(MoveNextQuestion());

            if(result.length <= trace){
                dispatch(PushAnswer("Real"))
            }
        
        }
    }

    // Timer functions
    
    const onCompleteTimeFun = () => {
        setFire_e(1)

        if(result.length <= trace){
            dispatch(PushAnswer("No Attempt"))
        }
        dispatch(DecreaseHealth());
        dispatch(MoveNextQuestion());
        setK((i) => !i);
    };

    const timerRenderer = ({ hours, minutes, seconds, completed }) => {
        return (
          <h5 className="timer text-light" style={{justifySelf:"end"}}>
            Time Left : {seconds} seconds
          </h5>
        );
      };

    

    return(
        <div className="container spacelayout">
            <div className="grid3">
                <h2 className="text-light">Identify Fake or Not</h2>
                
                <h2 className="text-light" style={{justifySelf:"center"}}>Your Score : {score}</h2>
            
{/*             
                <Countdown
                        
                        key={k}
                        date={Date.now() + 60000}
                        intervalDelay={0}
                        precision={3}
                        renderer={timerRenderer}
                        onComplete={onCompleteTimeFun}
                    /> */}
            </div>
            <div className="grid">
                <ProgressBar></ProgressBar>
                <E_ProgressBar></E_ProgressBar>
            </div>

            <div  className="grid3">
                <img src={SS1} alt="Space Ship 1" className="images"/>
                <div className="fire_grid">
                    <div className="Fire" onAnimationEnd={() => setFire(0)} fire={fire}></div>
                    <div className="Fire_E" onAnimationEnd={() => setFire_e(0)} fire_e={fire_e}></div>
                </div>
                <img src={SS2} alt="Space Ship 2" className="images" />
            </div>

            {/* <div className="sun"></div> */}


           
            

            {/* display questions */}
            
            <Questions/>
            
            

            <div className="grid">
                <button className="btn next" onClick={onFake} >{queue[trace]?.options[0]}</button>
                <button className="btn prev" onClick={onNotFake} >{queue[trace]?.options[1]}</button>
            </div>
           

        </div>
        
        
    )
}   