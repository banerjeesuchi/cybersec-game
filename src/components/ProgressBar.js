import React, { useEffect,useState } from "react";
import { useSelector,useDispatch } from 'react-redux'

export default function ProgressBar() {
    const bgcolor  = "#ef6c00";

    const game = useSelector(state => state.game.game)
    const g1_health = useSelector(state => state.questions.health);
    const g2_health = useSelector(state => state.story_line_questions.health);

    let health
    if(game === 'FakeBook')
    {
      health = g1_health
    }
    else if(game === 'StoryLine')
    {
      health = g2_health
    }
    
  
    const containerStyles = {
      height: 40,
      width: '30%',
      backgroundColor: "#e0e0de",
      borderRadius: 10,
      margin: 50,
      justifySelf:"center",
    }
  
    const fillerStyles = {
      height: '100%',
      width: `${health}%`,
      backgroundColor: bgcolor,
      borderRadius: 'inherit',
      textAlign: 'right',
      transition: 'width 1s ease-in-out',
    }
  
    const labelStyles = {
      padding: 20,
      color: 'black',
      fontWeight: 'bold',

    }
  
    return (
      <div style={containerStyles}>
        <div style={fillerStyles}>
          <span style={labelStyles}>{`${health}%`}</span>
        </div>
      </div>
    );
  };
  