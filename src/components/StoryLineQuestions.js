import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";

/**Custom Hook  */
import { useFetchStoryLineQuestion } from "../hooks/FetchStoryLineQuestions";
import { updateResult } from "../hooks/setResult";


export default function StoryLineQuestions() {

    
    const state = useSelector(state => state)

    const [{isLoading,apiData,serverError},] = useFetchStoryLineQuestion()
    const { queue, trace, score, health } = useSelector(state => state.story_line_questions);


    const questions = useSelector(state => state.story_line_questions.queue[state.story_line_questions.trace])
    const dispatch = useDispatch()

    // useEffect(() => {
        // dispatch(updateResult({ trace, checked }))
        // console.log(queue[trace])
    // })

    
    if(isLoading) return <h3 className="text-light">Loading</h3>
    if(serverError) return <h3 className="text-light">{serverError || "Unknown Error"}</h3>

    let StoryRen
    if(queue[trace]?.image !== ''){
        console.log(queue[trace]?.image)
        StoryRen = <div className="grid">
                        <p className="text-light"> {questions?.story}</p>
                        <img src={ queue[trace]?.image } className="questionimages"/>
                    </div>
    }
    else{
        StoryRen = <p className="text-light"> {questions?.story}</p>
    }

    return(
        
        <div className="questions">
            <h5 className="text-light">Story :</h5>
            {StoryRen}
            <h5 className="text-light">Question :</h5>
            <p className="text-light"> {questions?.question}</p>
        </div>
    )
}   