import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import '../styles/Result.css';
import ResultTable from "./ResultTable";
import ProgressBar from "./ProgressBar";

import { useDispatch, useSelector } from "react-redux";

/**Import actions */
import { resetAllAction } from "../redux/question_reducer";
import { resetStoryLineAllAction } from "../redux/story_line_question_reducer";
import { resetResultAction } from "../redux/result_reducer";
import { resetGame } from "../redux/gameselection_reducer";


export default function Result() {

    const dispatch = useDispatch()
    const game = useSelector(state => state.game.game)
    const g1_score = useSelector(state => state.questions.score);
    const g2_score = useSelector(state => state.story_line_questions.score);


    let score
    if(game === 'FakeBook')
    {
      score = g1_score
    }
    else if(game === 'StoryLine')
    {
      score = g2_score
    }
    


    let result_msg
    function onReturnHome(){
        dispatch(resetGame())
        if(game === 'FakeBook')
        {
            dispatch(resetAllAction())
        }
        else if(game === 'StoryLine')
        {
            dispatch(resetStoryLineAllAction())
        }
        dispatch(resetResultAction())
        
    }

    if(score >= 100 ){
        result_msg =  <h1 className="title text-light">Congratulations!!! You Won!!!</h1>
    }
    else{
        result_msg = <h1 className="title text-light">You can Improve!!!</h1>
    }

    return(
        <div className="container">
           
           
           {result_msg}

            <ProgressBar></ProgressBar>

            <h1 className="title text-light">Your Score : {score}</h1>
        

            <ResultTable></ResultTable>

            <div className="start">
                <Link className="btn" to={'/'} onClick={onReturnHome}>Return to Home</Link>
            </div>
            
        </div>
    )
}   