import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"

import * as Action from '../redux/gameselection_reducer'

export const SetGame = (game) => async(dispatch) =>{
    try{
        await dispatch(Action.setGameAction(game))
    }catch(error){
        console.log(error)
    }
    
}

