

/** fetch question hook to fetch api data and set value to store */

import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import data from "../database/data"

/** redux action */
import * as Action from '../redux/question_reducer'

export const useFetchQuestion = () =>{

    const dispatch = useDispatch();

    const [getData, setGetData ] = useState({ isLoading : false, apiData : [], serverError: null})

    useEffect(()=>{
        setGetData(prev=>({...prev, isLoading : true}));

        /**async funtion fetch backend data */
        (async () => {
            try{
                let fake_question,not_fake_question
                let question = await data;
                if(question.length > 0){
                    fake_question = question.filter(item => item.answer === 'Fake').slice(0,5)
                    not_fake_question = question.filter(item => item.answer === 'Real').slice(0,5)
                    
                    question = fake_question.concat(not_fake_question).sort(function () {
                        return Math.round(Math.random()) - 0.5
                    })

                    setGetData(prev=>({...prev, isLoading : false}));
                    setGetData(prev=>({...prev, apiData : question}));

                    /** dispatch an action */
                    dispatch(Action.startExamAction(question));
                }
                else{
                    throw new Error("No Question Available");
                }
            } catch (error) {
                setGetData(prev=>({...prev, isLoading : false}));
                setGetData(prev=>({...prev, serverError : error}));
            }
        })();
    }, [dispatch]);

    return [getData,setGetData]
}


/**MoveAction Dispatch function */

export const MoveNextQuestion = () => async (dispatch) => {
    try{
        dispatch(Action.moveNextAction());
    } catch(error){
        console.log(error)
    }
}


export const IncrementScore = () => async (dispatch) => {
    try{
        dispatch(Action.incrementScore());
    } catch(error){
        console.log(error)
    }
}

export const DecreaseHealth = () => async (dispatch) => {
    try{
        dispatch(Action.decreaseHealth());
    } catch(error){
        console.log(error)
    }
}