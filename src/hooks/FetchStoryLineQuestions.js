

/** fetch question hook to fetch api data and set value to store */

import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import data from "../database/story_line"

/** redux action */
import * as Action from '../redux/story_line_question_reducer'

export const useFetchStoryLineQuestion = () =>{

    const character = useSelector(state => state.story_line_character.character)

    // console.log(character)

    const dispatch = useDispatch();

    const [getData, setGetData ] = useState({ isLoading : false, apiData : [], serverError: null})

    useEffect(()=>{
        setGetData(prev=>({...prev, isLoading : true}));

        /**async funtion fetch backend data */
        (async () => {
            try{
                let question = await data;
                if(question.length > 0){
                    if(character === "G")
                    {
                        question = question.filter(item => item.storyline === 'Jewelry')
                    }
                    else{
                        question = question.filter(item => item.storyline === 'Space')
                    }
                    console.log(question)

                    setGetData(prev=>({...prev, isLoading : false}));
                    setGetData(prev=>({...prev, apiData : question}));

                    /** dispatch an action */
                    dispatch(Action.startStoryLineAction(question));
                }
                else{
                    throw new Error("No Question Available");
                }
            } catch (error) {
                setGetData(prev=>({...prev, isLoading : false}));
                setGetData(prev=>({...prev, serverError : error}));
            }
        })();
    }, [dispatch]);

    return [getData,setGetData]
}


/**MoveAction Dispatch function */

export const MoveNextQuestion = () => async (dispatch) => {
    try{
        dispatch(Action.moveNextAction());
    } catch(error){
        console.log(error)
    }
}


export const IncrementScore = () => async (dispatch) => {
    try{
        dispatch(Action.incrementScore());
    } catch(error){
        console.log(error)
    }
}

export const DecreaseHealth = () => async (dispatch) => {
    try{
        dispatch(Action.decreaseHealth());
    } catch(error){
        console.log(error)
    }
}