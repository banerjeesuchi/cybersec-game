import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"

import * as Action from '../redux/story_line_character_reducer'

export const SetStoryLineCharacter = (character) => async(dispatch) =>{
    try{
        await dispatch(Action.setStoryLineCharacterAction(character))
    }catch(error){
        console.log(error)
    }
    
}

