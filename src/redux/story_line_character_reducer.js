import { createSlice } from "@reduxjs/toolkit";

export const story_line_character_reducer = createSlice({
    name: 'story_line_character',
    
    initialState: {
        character: null,
    },

    reducers : {
        setStoryLineCharacterAction : (state, action) => {
                state.character = action.payload
        },
        resetStoryLineCharacter : ()=>{
            return{
                character: "",
            }
        },
    },
})

export const { setStoryLineCharacterAction,resetStoryLineCharacter } = story_line_character_reducer.actions;

export default story_line_character_reducer.reducer;