import { createSlice } from "@reduxjs/toolkit";

export const story_line_question_reducer = createSlice({
    name: 'story_line_questions',
    
    initialState: {
        queue: [],
        trace: 0,
        score: 0,
        health: 100
    },

    reducers : {
        startStoryLineAction : (state, action) => {
            return {
                ...state,
                queue : action.payload
            }
        },
        moveNextAction : (state,action) => {
            return{
                ...state,
                trace : state.trace + 1
            }
        },
        incrementScore : (state,action) => {
            return{
                ...state,
                score : state.score + 20
            }
        },

        decreaseHealth : (state,action) => {
            return{
                ...state,
                health : state.health - 20
            }
        },
        resetStoryLineAllAction : ()=>{
            return{
                queue: [],
                trace: 0,
                score:0,
                health: 100
            }
        },
    },
})

export const { startStoryLineAction, moveNextAction, incrementScore, decreaseHealth, resetStoryLineAllAction } = story_line_question_reducer.actions;

export default story_line_question_reducer.reducer;