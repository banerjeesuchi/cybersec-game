import { combineReducers, configureStore } from '@reduxjs/toolkit';
import gameselection_reducer from './gameselection_reducer';

/**Call Reducers */

import questionReducer from './question_reducer';
import resultReducer  from './result_reducer';
import story_line_character from './story_line_character_reducer';
import story_line_question_reducer from './story_line_question_reducer';

const rootReducer = combineReducers({
    game: gameselection_reducer,
    questions : questionReducer,
    result : resultReducer,
    story_line_character : story_line_character,
    story_line_questions : story_line_question_reducer
})


/** Create Store with reducer */

export default configureStore({ reducer : rootReducer })