import { createSlice } from "@reduxjs/toolkit"

/** create reducer */
export const questionReducer = createSlice({
    name: 'questions',
    
    initialState: {
        queue: [],
        answers : [],
        trace: 0,
        score: 0,
        health: 100
    },
    
    reducers : {
        startExamAction : (state, action) => {
            return {
                ...state,
                queue : action.payload
            }
        },
        moveNextAction : (state,action) => {
            return{
                ...state,
                trace : state.trace + 1
            }
        },
        movePrevAction : (state,action) => {
            return{
                ...state,
                trace : state.trace - 1
            }
        },
        resetAllAction : ()=>{
            return{
                queue: [],
                answers : [],
                trace: 0,
                score:0,
                health: 100
            }
        },

        incrementScore : (state,action) => {
            return{
                ...state,
                score : state.score + 20
            }
        },

        decreaseHealth : (state,action) => {
            return{
                ...state,
                health : state.health - 20
            }
        },
    }
});

export const { startExamAction, moveNextAction, movePrevAction, resetAllAction, incrementScore, decrementScore, decreaseHealth } =  questionReducer.actions;

export default questionReducer.reducer;
