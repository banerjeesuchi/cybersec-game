import { createSlice } from "@reduxjs/toolkit";

export const gameselection_reducer = createSlice({
    name: 'gameselection',
    
    initialState: {
        game: null,
    },

    reducers : {
        setGameAction : (state, action) => {
                state.game = action.payload
        },
        resetGame : ()=>{
            return{
                game: null,
            }
        },
    },
})

export const { setGameAction,resetGame } = gameselection_reducer.actions;

export default gameselection_reducer.reducer;