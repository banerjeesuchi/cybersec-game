export default [
    {
        id : 1,
        storyline : "Jewelry",
        story : "Shriya is a teenager girl and is just learning about social media sites, often spends her time on systems in public network to learn about the same. While on one of the social media website, she saw a marketing advertisement and got tempted to buy online Jewelry. She selected the jewelry and accessories she wanted to purchase and started to use her mother's credit card to pay online at http://PayforAnyEthing.com",
        question : "It is okay to buy online. But was it really okay for Shriya to buy from those websites? Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "",
        answer : "Not Safe",
        yes_text : "Yay! You're right. Website was not secured to start with Https://",
        no_text : "Oh Ho - No - website was not secured - Be careful - Enemy is getting strong and may defeat you."
    },

    {
        id : 2,
        storyline : "Jewelry",
        story : "But since Shriya was not aware of the fake website advertisement, she continued to use mother's credit card. Added Card details and additional information like  Full Name, Date of Birth, Card expiry date, CVV, selected the range of credit limit card had and also enabled for the ONE CLICK TRANSACTION.",
        question : "Was it safe for Shriya to fill all those information and enable the one click transaction which means this will go through every time from that phone or machine without much verification? Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "",
        answer : "Not Safe",
        yes_text : "Yay! You're right. It's not safe to SAVE card information or any other personal data in the unsafe websites nor to enable the one click transaction when we know it will be used by multiple users.",
        no_text : "- Oh no! You just allowed the access to personal details and card information which should not be shared on the unsafe websites nor we should enable the one click transaction when we know it will be used by multiple users."

    },
    
    {
        id : 3,
        storyline : "Jewelry",
        story : "But since Shriya was not aware that it wasn’t safe to share these information, she continued to proceed with the payment by saving card information and it redirected them to payment gateway which was from PalPay",
        question : "Was it safe for Shriya to fill the card information and cvv and to proceed with checkout at this payment gateway? Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "/Images/PalPay.png",
        answer : "Not Safe",
        yes_text : "Yay! You're right. This wasn’t an approved payment gateway, the word is misspelled and color representation is falsified. Some of the secure payment gateway that is recognized currently are below (but subject to change - so please consider to stay updated)",
        no_text : "NO! This wasn’t an approved payment gateway, the word is misspelled and color representation is falsified. Some of the secure payment gateway that is recognized currently are below (but subject to change - so please consider to stay updated)"

    },
    
    {
        id : 4,
        storyline : "Jewelry",
        story : "But since Shriya was not aware that it was fake they completed the checkout and paid for online purchase.  Shriya was super excited about their purchase and wanted rush back to home to tell their friends on what they purchased. So in hurry they forgot to log out from the system or phone which was on public network.",
        question : "Was it okay for Shriya to not log out from the system or phone which was on public network?  Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "",
        answer : "Not Safe",
        yes_text : "Yay! You're right.  Since they were in cyber centre / public network it's important to log out immediately otherwise someone else can continue to use the same system or has information thru the network and can continue from where there to use it further.",
        no_text : "NO!  Now that they didn't log out immediately someone else can continue to use the same system or has information thru the network and can continue from where there to use it further."

    },
    
    {
        id : 5,
        storyline : "Jewelry",
        story : "To their surprise their mother got a message or notification of her credit card usage multiple times, and the card was hacked. So the act of not being careful caused financial loss. They immediately had to block her card and file a cybercrime complaint online at https://cybercrime.gov.in/ or offline. Also go to nearest police station and file an FIR with all the details. They learnt lesson that they like to share with everyone all 1. Not to make payment when the link / url are not secured or safe and if doesn’t begin with https:// 2. Not to share a lot of unnecessary personal details and save them for future use when it's used by multiple users. 3. Not to enable ONE CLICK TRANSACTION when it's used by multiple people. 4. To pay attention to payment gateway before checkout like misspelled words, false logo, fake account etc., 3. To log off out of account / website when it's not personal phone or laptop.",
        question : "Was the story interesting and will you share this learning with your friends and family? Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "",
        answer : "Safe",
        yes_text : "This kind of situation happens to many if we are not careful, so this is just a small attempt to create awareness and we will be better next time.",
        no_text : "Thank you and we wish that this kind of situations don’t occur to any of you or your family and friends and its only possible when we spread them this awareness. Thanks again! Have a good one."

    },
    

    {
        id : 6,
        storyline : "Space",
        story : "Shreyansh is a boy and is just learning about social media sites, often spends her time on systems in public network to learn about the same. While on one of the social media website, she saw a marketing advertisement and got tempted to buy online Game. he selected the jewelry and accessories he wanted to purchase and started to use her mother's credit card to pay online at http://PayforAnyEthing.com",
        question : "It is okay to buy online. But was it really okay for Shreyansh to buy from those websites? Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "",
        answer : "Not Safe",
        yes_text : "Yay! You're right. Website was not secured to start with Https://",
        no_text : "Oh Ho - No - website was not secured - Be careful - Enemy is getting strong and may defeat you."
    },

    {
        id : 7,
        storyline : "Space",
        story : "But since Shreyansh was not aware of the fake website advertisement, he continued to use mother's credit card. Added Card details and additional information like  Full Name, Date of Birth, Card expiry date, CVV, selected the range of credit limit card had and also enabled for the ONE CLICK TRANSACTION.",
        question : "Was it safe for Shreyansh to fill all those information and enable the one click transaction which means this will go through every time from that phone or machine without much verification? Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "",
        answer : "Not Safe",
        yes_text : "Yay! You're right. It's not safe to SAVE card information or any other personal data in the unsafe websites nor to enable the one click transaction when we know it will be used by multiple users.",
        no_text : "- Oh no! You just allowed the access to personal details and card information which should not be shared on the unsafe websites nor we should enable the one click transaction when we know it will be used by multiple users."

    },
    
    {
        id : 8,
        storyline : "Space",
        story : "But since Shreyansh was not aware that it wasn’t safe to share these information, he continued to proceed with the payment by saving card information and it redirected them to payment gateway which was from PalPay",
        question : "Was it safe for Shreyansh to fill the card information and cvv and to proceed with checkout at this payment gateway? Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "/Images/PalPay.png",
        answer : "Not Safe",
        yes_text : "Yay! You're right. This wasn’t an approved payment gateway, the word is misspelled and color representation is falsified. Some of the secure payment gateway that is recognized currently are below (but subject to change - so please consider to stay updated)",
        no_text : "NO! This wasn’t an approved payment gateway, the word is misspelled and color representation is falsified. Some of the secure payment gateway that is recognized currently are below (but subject to change - so please consider to stay updated)"

    },
    
    {
        id : 9,
        storyline : "Space",
        story : "But since Shreyansh was not aware that it was fake they completed the checkout and paid for online purchase.  Shreyansh was super excited about their purchase and wanted rush back to home to tell their friends on what they purchased. So in hurry they forgot to log out from the system or phone which was on public network.",
        question : "Was it okay for Shreyansh to not log out from the system or phone which was on public network?  Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "",
        answer : "Not Safe",
        yes_text : "Yay! You're right.  Since they were in cyber centre / public network it's important to log out immediately otherwise someone else can continue to use the same system or has information thru the network and can continue from where there to use it further.",
        no_text : "NO!  Now that they didn't log out immediately someone else can continue to use the same system or has information thru the network and can continue from where there to use it further."

    },
    
    {
        id : 10,
        storyline : "Space",
        story : "To their surprise their mother got a message or notification of her credit card usage multiple times, and the card was hacked. So the act of not being careful caused financial loss. They immediately had to block her card and file a cybercrime complaint online at https://cybercrime.gov.in/ or offline. Also go to nearest police station and file an FIR with all the details. They learnt lesson that they like to share with everyone all 1. Not to make payment when the link / url are not secured or safe and if doesn’t begin with https:// 2. Not to share a lot of unnecessary personal details and save them for future use when it's used by multiple users. 3. Not to enable ONE CLICK TRANSACTION when it's used by multiple people. 4. To pay attention to payment gateway before checkout like misspelled words, false logo, fake account etc., 3. To log off out of account / website when it's not personal phone or laptop.",
        question : "Was the story interesting and will you share this learning with your friends and family? Click on Thumbs up if yes and Red Alert if No within 20 seconds.",
        options : [
            "👹","👍",
        ],
        image: "",
        answer : "Safe",
        yes_text : "This kind of situation happens to many if we are not careful, so this is just a small attempt to create awareness and we will be better next time.",
        no_text : "Thank you and we wish that this kind of situations don’t occur to any of you or your family and friends and its only possible when we spread them this awareness. Thanks again! Have a good one."

    },
   
]