export default [
    {
        id : 1,
        story : "Vandy lost her job during pandemic and was looking for through social media and other sites, She has been contacted by an employer and they have sent a mail asking her to apply for the job by clicking link provided on it.",
        message : "Could you please help Vandy on understanding that the mail sent to her is Fake or Real?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q1_new.png',
        answer : "Fake"

    },

    {
        id : 2,
        story: "Olivia has ordered an iPhone for her husband as a surprise gift and waiting for the parcel eagerly. One day she received a text from agency asking to confirm the delivery address.",
        message : "Message received by Olivia for her Parcel asking to confirm address is Fake or Real?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q2_new.png',
        answer : "Fake"
    },

    {
        id : 3,
        story: "Liza received fake text message, claiming to be from Google Gmail security support, and tells her to expect a shortly-forthcoming recovery code via SMS from another phone number, which needs to be sent back in reply to the message. Then hacker then starts a login attempt at your legitimate service, but then acts like they do not know the right password. Then the attacker tells the service to send them an SMS “recovery code”  and the legitimate recovery code gets sent from the service to the Liza’s previously registered.",
        message : "Should Liza retype the PIN and send it back to requester, Kindly help her to understand that message  is Fake or Real?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q3_new.png',
        answer : "Fake"
    },

    {
        id : 4,
        story: "Ana has got her shift off and  in all her mood to binge watch on Netflix today .By the time she starts the app and realized its not working. Next moment she received a mail from from Netflix team stating account is on hold due to non-payment.",
        message : "Will Ana continue to do payment of her Netflix from the mail above,does it look like real to her?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q4_new.png',
        answer : "Fake"
    },

    {
        id : 5,
        story: "Sean is a Flipkart customer and he has started getting continuous messages from them regarding Lucky draw coupon and has to click the links to claim the amount.",
        message : "Will Sean be able to identify the source of information is real or fake ?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q5_new.png',
        answer : "Fake"
    },

    {
        id : 6,
        story: "Jiya got  a WhatsApp text  telling she won 35 lakh INR in KBC .Here is the picture of the text.",
        message : "Will Jiya be able to claim the amount from lottery or it’s a Scam?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q6_F.png',
        answer : "Fake"
    },
    {
        id : 7,
        story: "Ria has received a SMS alert warning that her card have been disabled, and that she must click on a link or call a phone number in order to secure her account.",
        message : "Could you please help Ria to identify the authenticity of SMS alert?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q7_F.png',
        answer : "Fake"
    },
    {
        id : 8,
        story: "Sini has received a forwarded message about Samsung Mobile Sale from Amazon. ",
        message : "Will Sini be able to identify that the message is Fake or Real?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q8_F.png',
        answer : "Fake"
    },
    {
        id : 9,
        story: "Gil has received a message asking to confirm the transaction which has been initiated from him and asking for confirmation from his side.",
        message : "Will Gil be able to identify that the message is Fake or Real?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q9_F.png',
        answer : "Fake"
    },


    {
        id : 10,
        story: "Rama got a mail from her friend “You can earn up to 51 referral rewards by inviting new users to Google Pay. Once the referred user makes their first payment, both users will receive the reward. You can only earn one referral reward for installing Google Pay”.",
        message : "Could you please help Rama on understanding whether the sent invitation through the mail is Fake or Real?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q10_R.png',
        answer : "Real"
    },
    {
        id : 11,
        story: "Black Friday is started one week early to stella as she get a promotional offer message from Rhone website.",
        message : "What is your thought on the early Black Friday's offer, Do you believe it is real or not",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q11_R.png',
        answer : "Real"
    },
    {
        id : 12,
        story: "Timothy got notified with a reminder regarding his left over cart. suggest him the best way in this situation.",
        message : "Would you suggest him to go with the link?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q12_R.png',
        answer : "Real"
    },
    {
        id : 13,
        story: "Lavanya has to pay the bills, Meanwhile, she got this message from Chaitanya that there she can pay her all bills and earn some money “Referrer when you pay your credit card bill on CRED, for every rupee cleared on your bill you earn a CRED coin. - you can then use earned CRED coins to claim exclusive rewards from different brands. CRED Gem Rewards: - for every person that you refer to CRED who makes a bill payment, you earn 10 gems”.",
        message : "What’s app message was received from Chaitanya for her country delight asking to confirm address is Fake or Real?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q13_R.png',
        answer : "Real"
    },
    {
        id : 14,
        story: "Krishna’s daughter was on the way to an exam and she forgot to bring her Geometry box, Krishna’s friend refer that there is a “Get 25% Discount on Inviting Friends | Refer and Earn ...  The members will get a rebate on online grocery shopping when they create a new account for once”.",
        message : "The message was received from Krishna’s friend for her exam stuff asking to confirm address is Fake or Real.",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q14_R.png',
        answer : "Real"
    },
    {
        id : 15,
        story: "Sushma’s Grandmother needs a tablet which is not available in nearby Market and she received WhatsApp notification about medical link” Accredited with - International Quality Certification, Apollo Pharmacy offers genuine medicines round-the-clock, through our 24-hour Pharmacies. Apollo Pharmacy also provides customer care any time of the day!”  ",
        message : "Can you help Sushma that the notification received through the medical link is Fake or Real?",
        options : [
            "Fake","Real",
        ],
        image: '/Images/q15_R.png',
        answer : "Real"
    },

    
]